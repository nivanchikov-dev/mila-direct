Pod::Spec.new do |s|

  s.name         = "MilaDirect"
  s.version      = "0.0.3"
  s.summary      = "Direct commands helper library for Mila project"
  s.license      = "Commercial"
  s.author       = { "Nikita Ivanchikov" => "nivanchikov.dev@gmail.com" }
  s.homepage     = "https://bitbucket.org/nivanchikov-dev/mila-direct"

  s.module_name = s.name

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source = { :git => "git@bitbucket.org:nivanchikov-dev/mila-direct.git", :tag => s.version.to_s }

  s.source_files  = "src/**/*.{swift,h,m}"
  s.framework  = "SystemConfiguration"

  s.dependency 'CocoaAsyncSocket', '~> 7'
  s.dependency 'ObjectMapper', '~> 1'
  s.dependency 'CocoaLumberjack/Swift', '~> 2.2'
  s.dependency 'Reachability', '~> 3.2'
end
