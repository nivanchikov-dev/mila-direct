//
//  mila-direct.h
//  mila-direct
//
//  Created by Nikita Ivanchikov on 11/24/15.
//  Copyright © 2015 Nikita Ivanchikov. All rights reserved.
//

#import <Foundation/Foundation.h>


//! Project version number for MilaDirect.
FOUNDATION_EXPORT double MilaDirectVersionNumber;

//! Project version string for MilaDirect.
FOUNDATION_EXPORT const unsigned char MilaDirectVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MilaDirect/PublicHeader.h>
#import <MilaDirect/IfAddr.h>