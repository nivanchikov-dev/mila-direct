//
//  MDDirectCommand.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/4/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public protocol CommandParams: Mappable
{
	static var type: CommandType {get};
}


public protocol UIRepresentable
{
	static var paramsList: [ParameterDescription] { get };
	
	init(arguments: [Any]) throws;
}


public enum CommandType: String
{
	case Unknown
	case SensorStatus = "upload_sensor_status";
	case DeviceSettings = "upload_device_settings";
	case FanSettings = "upload_fan_settings"
	case QuerySystemStatus = "query_system_status";
	case GetDeviceUID = "get_uid";
	case FactoryReset = "factory_reset";
	case DeviceReset = "reset_reset"
	case SynchroniseTime = "synchronise_time"
	case PowerMode = "power"
	case ScreenAutoOff = "screen_timer_off"
    case ScreenAutoDim = "display_auto_dim"
	case SleepMode = "sleep_mode"
	case DeviceNumber = "device_number";
	case Schedule = "schedule"
	case FanSpeed = "fan_speed_setting"
	case ChildLock = "child_lock"
	case AuthorizeDevice = "authorize_device"
	case ClearFilterTime = "clear_filter_time"
	case AQIxFactor = "aqi_factor_x_setting"
	case FanLevels = "fan_level_setting"
	case GetFirmwareVersion = "get_firmware_version"
	case UpdateDeviceFirmware = "update_firmware"
	
	
	public static let allValues: [CommandType] = [.QuerySystemStatus, .GetDeviceUID, .FactoryReset, .DeviceReset, .SynchroniseTime, .PowerMode, .ScreenAutoOff, .ScreenAutoDim, .SleepMode, .DeviceNumber, .Schedule, .FanSpeed, .ChildLock, .AuthorizeDevice, .ClearFilterTime, .AQIxFactor, .FanLevels, .GetFirmwareVersion, .UpdateDeviceFirmware];
}


public enum CommandResult: String
{
	case Unknown
	case Success = "OK"
	case Failure = "NG"
}


public enum ParameterType
{
	case Bool
	case Range(min: Int, max: Int)
	case Values(values: [Int])
	case Time
}


public struct Parameter<T>: CustomStringConvertible
{
	public var value: T;
	var type: ParameterType;
	
	init(value: T, type: ParameterType)
	{
		self.value = value;
		self.type = type;
	}
	
	
	public var description: String
	{
		return "\(value)";
	}
}


public struct MDDirectCommand<T: CommandParams>: Mappable
{
	var type: CommandType = .Unknown;
	var deviceUID: String?;
	var fallbackIP: String?;
	var params: T?;
	
	
	public init(deviceUID: String, params: T, fallbackIP: String? = nil)
	{
		self.type = T.type;
		self.deviceUID = deviceUID;
		self.params = params;
		self.fallbackIP = fallbackIP;
	}
	
	
	public init(deviceUID: String, type: CommandType, fallbackIP: String? = nil)
	{
		self.type = type;
		self.deviceUID = deviceUID;
		self.fallbackIP = fallbackIP;
	}
	
	
	public init?(_ map: Map)
	{
		
	}
	
	
	mutating public func mapping(map: Map)
	{
		type <- map["command"];
		deviceUID <- map["uid"];
		params <- (map["args"], transformOfParams);
	}
	
	
	private var transformOfParams: TransformOf<T, AnyObject>
	{
		return TransformOf<T, AnyObject>(fromJSON: { (anObject) -> T? in
			return Mapper<T>().map(anObject);
		},
		toJSON: { (params) -> AnyObject? in
			if let _ = params {
				return Mapper<T>().toJSON(params!);
			}
			
			return nil;
		});
	}
}


public extension CommandType
{
	var name: String?
	{
		switch self
		{
			case .QuerySystemStatus:
				return "System status (get)";
			case .GetDeviceUID:
				return "Device UID (get)";
			case .FactoryReset:
				return "Factory reset (->)";
			case .DeviceReset:
				return "Device reset (->)";
			case .SynchroniseTime:
				return "Synchronise time (set)";
			case .PowerMode:
				return "Power mode (set)";
			case .ScreenAutoOff:
				return "Screen auto off (set)";
            case .ScreenAutoDim:
                return "Screen auto-dim (set)";
			case .SleepMode:
				return "Sleep mode (set)";
			case .DeviceNumber:
				return "Device number (set)";
			case .Schedule:
				return "Schedule (set)";
			case .FanSpeed:
				return "Fan speed (set)";
			case .ChildLock:
				return "Child lock (set)";
			case .AuthorizeDevice:
				return "Authorize device (set)";
			case .ClearFilterTime:
				return "Clear filter time (set)";
			case .AQIxFactor:
				return "AQI X Factor (set)";
			case .FanLevels:
				return "Fan levels (set)";
			case .GetFirmwareVersion:
				return "Firmware version (get)";
			case .UpdateDeviceFirmware:
				return "Update device firmware (->)";
			default:
				return nil;
		}
	}
	
	var paramsList: [ParameterDescription]
	{
		switch self
		{
			case .PowerMode:
				return PowerModeParams.paramsList;
			case .ChildLock:
				return ChildLockParams.paramsList;
			case .DeviceNumber:
				return DeviceNumberParams.paramsList;
			case .ScreenAutoOff:
				return ScreenAutoOffParams.paramsList;
            case .ScreenAutoDim:
                return ScreenAutoDimParams.paramsList;
			case .SleepMode:
				return SleepModeParams.paramsList;
			case .FanSpeed:
				return FanSpeedParams.paramsList;
			case .AuthorizeDevice:
				return AuthorizeDeviceParams.paramsList;
			case .AQIxFactor:
				return AQIFactorParams.paramsList;
			case .Schedule:
				return ScheduleParams.paramsList;
			case .FanLevels:
				return FanLevelsParams.paramsList;
			case .SynchroniseTime:
				return SynchroniseTimeParams.paramsList;
			default:
				return [ParameterDescription]();
		}
	}
}
