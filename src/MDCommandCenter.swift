//
//  MDCommandCenter.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/6/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper
import CocoaLumberjack
import Reachability


public protocol MDCommandCenterDelegate: class
{
    func commandCenterDidUpdateOnlineDevices(devices: [String]);
    func commandCenterDidReceiveResponse(response: MDDirectResponse, fromDevice deviceUID: String);
}


public extension MDCommandCenterDelegate
{
    func commandCenterDidUpdateOnlineDevices(devices: [String]) {}
    func commandCenterDidReceiveResponse(response: MDDirectResponse, fromDevice deviceUID: String) {}
}


public final class MDCommandCenter
{
    public weak var delegate: MDCommandCenterDelegate? = nil;
    
    private let devicesQueue = dispatch_queue_create("com.mila.debugger.devicesQueue", DISPATCH_QUEUE_CONCURRENT);
    private let delegateQueue = dispatch_queue_create("com.mila.debugger.commandQueue", DISPATCH_QUEUE_CONCURRENT);
    
    
    private var commandQueue = NSOperationQueue();
    private var reachability = Reachability.reachabilityForLocalWiFi();
    
    
    private var devices = Set<MDOnlineDevice>();
    private var responding = Set<MDOnlineDevice>();
    private var sockets = Set<MDTCPSocket>();
    private var udpSocket: MDUDPSocket!;
    
    
    public var onlineDevices: Set<MDOnlineDevice>
    {
        var devices: Set<MDOnlineDevice>!;
        
        dispatch_barrier_sync(devicesQueue) {
            devices = self.devices;
        }
        
        return devices;
    }
    
    
    private var respondingDevices: Set<MDOnlineDevice>
    {
        var devices: Set<MDOnlineDevice>!;
        
        dispatch_barrier_sync(devicesQueue) {
            devices = self.responding;
        }
        
        return devices;
    }
    
    
    public var logLevel: DDLogLevel = .Verbose
        {
        didSet
        {
            udpSocket?.logLevel = .Info;//logLevel;
            
            var allSockets: Set<MDTCPSocket>!;
            
            dispatch_barrier_sync(devicesQueue) {
                allSockets = self.sockets;
            }
            
            for socket in allSockets {
                socket.logLevel = logLevel;
            }
        }
    }
    
    
    // MARK: - Lifecycle
    
    public init(udpPassword: String = "mila_bc_6444552", udpPort: Int = 48899) throws
    {
        try udpSocket = MDUDPSocket(password: udpPassword, port: udpPort, heartbeatInterval: 3.0);
        
        udpSocket.delegate = self;
        
        if (reachability.isReachableViaWiFi()) {
            try udpSocket.startHeartbeat();
        }
        
        reachability.reachableBlock = { [weak self] (r) in
            if r.isReachableViaWiFi() {
                _ = try? self?.udpSocket.startHeartbeat();
            } else {
                self?.handleUnreachableState();
            }
        }
        
        reachability.unreachableBlock = { [weak self] (_) in
            self?.handleUnreachableState();
        }
        
        reachability.startNotifier();
    }
    
    
    func handleUnreachableState()
    {
        if udpSocket.socketState == HearbeatSocketState.Idle {
            return;
        }
        
        udpSocket.stopHeartbeat();
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC) * Int64(udpSocket.heartbeatInterval)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.filterOutOfflineDevices(true);
        });
    }
    
    
    public func teardown()
    {
        cancelAllOperations();
        
        filterOutOfflineDevices(force: true);
        udpSocket.stopHeartbeat();
        reachability.stopNotifier();
    }
    
    
    deinit
    {
        
    }
    
    
    // MARK: - Actions
    
    private func onlineDeviceForUID(deviceUID: String) -> MDResult<MDOnlineDevice, MilaDirectError>
    {
        let device: MDOnlineDevice? = onlineDevices.filter{ $0.mac == deviceUID }.last;
        
        guard let _ = device else {
            DDLogError("Device not found in current network \(deviceUID)", level: logLevel);
            return MDResult(error: MilaDirectError.DeviceNotOnline(deviceUID: deviceUID)) ;
        }
        
        return MDResult(value: device!);
    }
    
    
    public func sendCommand<T: CommandParams>(command: MDDirectCommand<T>, force: Bool = false, completion: ((result: MDResult<Any, NSError>) -> ())? = nil) -> NSOperation?
    {
        var operation: MDCommandOperation<T>?;
        
        do
        {
            let result = onlineDeviceForUID(command.deviceUID!);
            
            var device: MDOnlineDevice!;
            
            if let existing = result.value {
                device = existing;
            } else if let deviceIP = command.fallbackIP where force == true {
                // use fallback ip for device in case a command needs to be sent w/o broadcast-determined device ip (e.g. during smartlink)
                device = MDOnlineDevice(ip: deviceIP, mac: command.deviceUID!);
            } else {
                throw result.error!;
            }
            
            let socket = try openSocketForDevice(device);
            
            operation = MDCommandOperation(command: command, socket: socket, logLevel: logLevel);
            
            operation!.completion = { (result) in
                completion?(result: result);
            }
            
            commandQueue.addOperation(operation!);
        }
        catch (let error)
        {
            DDLogError("Failed to send command \(command) to \(command.deviceUID) due to \(error)", level: logLevel);
            
            let result = MDResult<Any, NSError>(error: error as NSError);
            completion?(result: result);
        }
        
        return operation;
    }
    
    
    public func cancelAllOperations()
    {
        commandQueue.cancelAllOperations();
    }
    
    
    // MARK: Private
    
    private func openSocketForDevice(device: MDOnlineDevice) throws -> MDTCPSocket
    {
        var allSockets: Set<MDTCPSocket>?;
        
        dispatch_barrier_sync(devicesQueue) {
            allSockets = self.sockets;
        }
        
        var socket: MDTCPSocket? = nil;
        
        if let _ = allSockets
        {
            for aSocket in allSockets!
            {
                guard (aSocket.isConnected) else {
                    continue;
                }
                
                guard let host = aSocket.connectedHost where aSocket.deviceUID == device.mac && host == device.ip else {
                    continue;
                }
                
                socket = aSocket;
                break;
            }
        }
        
        if let _ = socket {
            //			DDLogDebug("Found connected socket for \(device.mac)", level: logLevel);
            return socket!;
        }
        
        socket = allSockets?.filter{ $0.isDisconnected }.last;
        
        if socket == nil {
            DDLogDebug("Creating socket for \(device.mac)", level: logLevel);
            socket = MDTCPSocket(deviceUID: device.mac, ipAddress: device.ip, delegateQueue: delegateQueue, logLevel: logLevel);
        } else {
            DDLogDebug("Found free socket for \(device.mac)", level: logLevel);
            socket?.updateWithDevice(device.mac, ipAddress: device.ip);
        }
        
        socket?.addListener(self);
        
        try socket?.connect();
        
        dispatch_barrier_async(devicesQueue) {
            self.sockets.insert(socket!);
        }
        
        return socket!;
    }
    
    
    public func closeSocketsForDevices(deviceIDs: [String])
    {
        var allSockets: Set<MDTCPSocket>?;
        
        dispatch_barrier_sync(devicesQueue) {
            allSockets = self.sockets;
        }
        
        guard let _ = allSockets else {
            return;
        }
        
        var socketsCopy = allSockets!;
        
        for mac in deviceIDs
        {
            for socket in allSockets!
            {
                if (socket.deviceUID != mac) {
                    continue;
                }
                
                DDLogDebug("Closing socket for \(mac)@\(socket.connectedHost)");
                
                socket.disconnect();
                socketsCopy.remove(socket);
            }
        }
        
        dispatch_barrier_async(devicesQueue) {
            self.sockets = socketsCopy;
        }
    }
    
    
    private func filterOutOfflineDevices(deleteAll: Bool = false, force: Bool = false)
    {
        var filter = false;
        
        if (force) {
            filter = true
        } else if (deleteAll) {
            filter = !reachability.isReachableViaWiFi();
        }
        
        // device is considered offline if it didn't respond for last 3 heartbeats
        let obsoleteInterval = udpSocket.heartbeatInterval * 3;
        let interval = NSDate().timeIntervalSince1970;
        
        let oDevices = onlineDevices;
        let prefilteredCount = oDevices.count;
        
        let devices = oDevices.filter{ (interval - $0.updated) < obsoleteInterval && !filter };
        let offlineDeviceIDs = oDevices.filter{ filter || (interval - $0.updated) >= obsoleteInterval }.map{ $0.mac };
        
        if offlineDeviceIDs.count > 0 {
            DDLogDebug("Closing sockets for offline devices: \(offlineDeviceIDs)");
            closeSocketsForDevices(offlineDeviceIDs);
        }
        
        dispatch_barrier_async(devicesQueue) { () -> Void in
            self.devices = Set(devices);
        }
        
        if (devices.count != prefilteredCount) {
            delegate?.commandCenterDidUpdateOnlineDevices(devices.map{ $0.mac });
        }
    }
    
    
    private func filterOutUnresponsiveDevices()
    {
        let obsoleteInterval = 5.0 * 3;
        let interval = NSDate().timeIntervalSince1970;
        
        let rDevices = respondingDevices;
        
        let devices = rDevices.filter{ (interval - $0.updated) < obsoleteInterval };
        let unresponsiveDeviceIDs = rDevices.filter{ (interval - $0.updated) >= obsoleteInterval }.map{ $0.mac };
        
        if unresponsiveDeviceIDs.count > 0 {
            DDLogDebug("Closing sockets for unresponsive devices: \(unresponsiveDeviceIDs)");
            closeSocketsForDevices(unresponsiveDeviceIDs);
        }
        
        dispatch_barrier_async(devicesQueue) { () -> Void in
            self.responding = Set(devices);
        }
    }
}


extension MDCommandCenter: MDUDPSocketDelegate
{
    // MARK: - UDP socket delegate
    
    internal func udpSocketSentHeartbeatMessage(socket: MDUDPSocket)
    {
        filterOutOfflineDevices();
    }
    
    
    internal func udpSocket(socket: MDUDPSocket, discoveredOnlineDevice device: MDOnlineDevice)
    {
        let existed = devices.contains(device);
        
        dispatch_barrier_async(devicesQueue) { () -> Void in
            self.devices.insert(device);
        }
        
        if !existed {
            delegate?.commandCenterDidUpdateOnlineDevices(onlineDevices.map{ $0.mac });
        }
        
        do
        {
            //			DDLogDebug("Device \(device.mac) found at \(device.ip)", level: self.logLevel);
            try self.openSocketForDevice(device);
        }
        catch (let error) {
            DDLogError("Failed to create socket for \(device.mac)@\(device.ip) due to \(error)", level: self.logLevel);
        }
    }
    
    
    internal func udpSocket(socket: MDUDPSocket, heartbeatFailedDueTo error: ErrorType)
    {
        if (reachability.isReachableViaWiFi()) {
            _ = try? socket.startHeartbeat();
        }
    }
}


// MARK: - TCP socket listener

extension MDCommandCenter: MDTCPSocketListener
{
    @objc internal func socket(sock: MDTCPSocket, didConnectToHost host: String!, port: UInt16)
    {
        
    }
    
    
    @objc internal func socket(sock: MDTCPSocket, didWriteDataWithTag tag: Int)
    {
        filterOutUnresponsiveDevices();
    }
    
    
    @objc internal func socket(sock: MDTCPSocket, didReadData data: NSData!, withTag tag: Int)
    {
        let device = MDOnlineDevice(ip: sock.ipAddress, mac: sock.deviceUID);
        
        dispatch_barrier_async(devicesQueue) { () -> Void in
            self.responding.insert(device);
        }
        
        do
        {
            let json = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments);
            
            DDLogInfo("Received data \(json) from socket \(sock.deviceUID)@\(sock.connectedHost)", level: logLevel);
            
            guard let response = Mapper<MDDirectResponse>().map(json) else {
                DDLogError("Failed to map response from \(sock.deviceUID)@\(sock.connectedHost)");
                return;
            }
            
            delegate?.commandCenterDidReceiveResponse(response, fromDevice: sock.deviceUID);
        }
        catch (let error) {
            DDLogError("Failed to deserialize response from \(sock.deviceUID)@\(sock.connectedHost) due to \(error)", level: logLevel);
        }
    }
    
    
    @objc internal func socketDidDisconnect(sock: MDTCPSocket, withError err: NSError!)
    {
        closeSocketsForDevices([sock.deviceUID]);
    }
}
