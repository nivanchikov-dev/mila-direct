//
//  IfAddr.h
//  mila-direct
//
//  Created by Nikita Ivanchikov on 11/24/15.
//  Copyright © 2015 Nikita Ivanchikov. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface IfAddr : NSObject

+ (nullable NSString*) networkMask;

@end
