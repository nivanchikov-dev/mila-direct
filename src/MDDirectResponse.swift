//
//  MDDirectResponse.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/6/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public class MDDirectResponse: NSObject, Mappable
{
	public var result: CommandResult?;
	public var type: CommandType = .Unknown;
	public var deviceUID: String?;
	public var args: [String: AnyObject]?;
	
	
	public required init?(_ map: Map)
	{
		
	}
	
	
	public func mapping(map: Map)
	{
		result <- map["command_res"];
		type <- map["command"];
		deviceUID <- map["uid"];
		args <- map["args"]
	}
	
	
	public func commandArgsOfType<T: Mappable where T: CommandParams>(_: T.Type) -> T?
	{
		return Mapper<T>().map(args);
	}
}
