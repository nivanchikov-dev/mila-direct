//
//  MDCommandOperation.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/6/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper;
import SystemConfiguration.CaptiveNetwork;
import CocoaLumberjack


enum State
{
	case Ready, Executing, Finished
	
	func keyPath() -> String {
		switch self {
		case Ready:
			return "isReady"
		case Executing:
			return "isExecuting"
		case Finished:
			return "isFinished"
		}
	}
}


class MDCommandOperation<T: CommandParams>: NSOperation, MDTCPSocketListener
{
	typealias Completion = (result: MDResult<Any, NSError>) -> ();
	
	var completion: (Completion)?;
	
	private weak var tcpSocket: MDTCPSocket?;
	private var command: MDDirectCommand<T>;
	private var buffer = NSMutableData();
	
	var logLevel: DDLogLevel;
	
	
	
	// MARK: - Managing asynchronousity
	
	override var asynchronous: Bool {
		return true;
	}
	
	
	var state = State.Ready {
		willSet {
			willChangeValueForKey(newValue.keyPath())
			willChangeValueForKey(state.keyPath())
		}
		didSet {
			didChangeValueForKey(oldValue.keyPath())
			didChangeValueForKey(state.keyPath())
		}
	}
	
	
	override var ready: Bool {
		return super.ready && state == .Ready
	}
	
	override var executing: Bool {
		return state == .Executing
	}
	
	override var finished: Bool {
		return state == .Finished
	}
	
	
	override func start()
	{
		if cancelled || !self.respondsToSelector(#selector(MDCommandOperation.run)) {
			state = .Finished;
			return;
		}
		
		state = .Executing;
		
		run();
	}
	
	
	private func finish ()
	{
		tcpSocket?.removeListener(self);
		
		state = .Finished;
	}
	
	
	internal func success<T>(result: T)
	{
		finish();
		
		let res: MDResult<Any, NSError> = .Success(result);
		
		if let c = completion {
			c(result: res);
		}
	}
	
	
	internal func failure(error: NSError)
	{
		finish();
		
		let res: MDResult<Any, NSError> = .Failure(error);
		
		if let c = completion {
			c(result: res);
		}
	}
	
	
	// MARK: - Lifecycle
	
	init(command: MDDirectCommand<T>, socket: MDTCPSocket, logLevel: DDLogLevel)
	{
		self.command = command;
		self.tcpSocket = socket;
		self.logLevel = logLevel;
		
		super.init();
		
		socket.addListener(self);
	}
	
	
	func run()
	{
		autoreleasepool ({
			
			guard let _ = tcpSocket else {
				finalizeWithResult(nil, error: NSError(domain: "", code: 0, userInfo: nil));
				return;
			}
			
			do
			{
				if (tcpSocket!.isDisconnected) {
					try tcpSocket!.connect();
				} else if (tcpSocket!.isConnected) {
					sendCommand();
				}
			}
			catch (let error) {
				finalizeWithResult(nil, error: error);
			}
		});
	}
	
	
	override func cancel()
	{
		super.cancel();

		finish();
	}
	
	
	private func finalizeWithResult(result: Any?, error: ErrorType?)
	{
		if (self.cancelled) {
			return;
		}
		
		if let _ = result {
			success(result!);
		} else {
			DDLogError("Failed to send command \(command) to device \(tcpSocket?.deviceUID) due to \(error)", level: logLevel);
			failure(error! as NSError);
		}
	}
	
	
	private func sendCommand()
	{
		let JSON = Mapper<MDDirectCommand<T>>().toJSON(command);
		
		if (self.cancelled) {
			return;
		}
		
		DDLogInfo("Sending command \(JSON) to device \(tcpSocket?.deviceUID):\(tcpSocket?.connectedHost)", level: logLevel);
		
		do
		{
			let data = try NSJSONSerialization.dataWithJSONObject(JSON, options: .PrettyPrinted);
			
			tcpSocket?.writeData(data, withTimeout: 10.0, tag: 1);
		}
		catch (let error) {
			finalizeWithResult(nil, error: error);
		}
	}
	
	
	// MARK: - GCDAsyncSocket delegate
	
	func socket(sock: MDTCPSocket, didConnectToHost host: String!, port: UInt16)
	{
		if (self.cancelled) {
			return;
		}
		
		sendCommand();
	}
	
	
	func socketDidDisconnect(sock: MDTCPSocket, withError err: NSError!)
	{
		if (self.cancelled) {
			return;
		}
		
		guard let e = err else {
			return;
		}
		
		finalizeWithResult(nil, error: e);
	}
	
	
	func socket(sock: MDTCPSocket, didWriteDataWithTag tag: Int)
	{
		finalizeWithResult(Void(), error: nil);
	}
}
