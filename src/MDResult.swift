//
//  MDResult.swift
//  mila-direct
//
//  Created by Nikita Ivanchikov on 11/24/15.
//  Copyright © 2015 Nikita Ivanchikov. All rights reserved.
//


public enum MDResult<Value, E where E: ErrorType>
{
	case Success(Value)
	case Failure(E)
	
	
	init(value: Value)
	{
		self = .Success(value);
	}
	
	
	init(error: E)
	{
		self = .Failure(error);
	}
	
	
	var isSuccess: Bool
	{
		switch self
		{
			case .Success:
				return true;
			case .Failure:
				return false;
		}
	}
	
	var value: Value?
	{
		switch self
		{
			case .Success(let value):
				return value;
			case .Failure:
				return nil;
		}
	}
	
	
	var error: E?
	{
		switch self
		{
			case .Success:
				return nil;
			case .Failure(let error):
				return error;
		}
	}
}

