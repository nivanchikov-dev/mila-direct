//
//  FanSpeedParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/5/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct FanSpeedParams: CommandParams, UIRepresentable
{
	public var fanSpeed: Parameter<Int>?;
	
	
	public init(fanSpeed: Int) throws
	{
		self.fanSpeed = Parameter(value: fanSpeed, type: FanSpeedParams.fanSpeedParamType);
	}
	
	
	static var fanSpeedParamType: ParameterType
	{
		return .Range(min: 0x01, max: 0x64);
	}
	
	
	public init?(_ map: Map)
	{
		mapping(map);
	}
	
	
	public mutating func mapping(map: Map)
	{
		fanSpeed <- (map["fan_speed"], transformOfParams(type: FanSpeedParams.fanSpeedParamType));
	}
	
	
	// MARK: - CommandParams protocol
	
	public static var type: CommandType
	{
		return .FanSpeed;
	}
	
	
	public static var paramsList: [ParameterDescription]
	{
		return [ParameterDescription(name: "Fan speed", type: fanSpeedParamType)];
	}
	
	
	public init(arguments: [Any]) throws
	{
		fanSpeed = Parameter(value: arguments[0] as! Int, type: FanSpeedParams.fanSpeedParamType);
	}
}
