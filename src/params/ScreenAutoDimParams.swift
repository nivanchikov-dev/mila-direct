//
//  ScreenAutoDimParams.swift
//  mila-direct
//
//  Created by Nikita Ivanchikov on 3/8/16.
//  Copyright © 2016 Nikita Ivanchikov. All rights reserved.
//


import ObjectMapper


public struct ScreenAutoDimParams: CommandParams, UIRepresentable
{
    public var lightThreshold: Parameter<Int>?;
    public var disabled: Parameter<Bool>?;
    
    
    public init(autoDimDisabled: Bool, lightThreshold: Int = 0x05) throws
    {
        self.disabled = Parameter(value: autoDimDisabled, type: ScreenAutoDimParams.autoDimDisabledParamType);
        self.lightThreshold = Parameter(value: lightThreshold, type: ScreenAutoDimParams.lightThresholdParamType);
    }
    
    
    static var autoDimDisabledParamType: ParameterType
    {
        return .Bool;
    }
    
    
    static var lightThresholdParamType: ParameterType
    {
        return .Range(min: 0x01, max: 0xFF);
    }
    
    
    public init?(_ map: Map)
    {
        mapping(map);
    }
    
    
    public mutating func mapping(map: Map)
    {
        disabled <- (map["display_auto_dim_function"], transformOfParams(type: ScreenAutoDimParams.autoDimDisabledParamType));
        lightThreshold <- (map["threshold_of_light_when_dim"], transformOfParams(type: ScreenAutoDimParams.lightThresholdParamType));
    }
    
    
    // MARK: - CommandParams protocol
    
    public static var type: CommandType
    {
        return .ScreenAutoDim;
    }
    
    public static var paramsList: [ParameterDescription]
    {
        return [ParameterDescription(name: "Light threshold for auto-dim", type: lightThresholdParamType),
            ParameterDescription(name: "Auto-dim enabled", type: autoDimDisabledParamType),];
    }
    
    
    public init(arguments: [Any]) throws
    {
        lightThreshold = Parameter(value: arguments[0] as! Int, type: ScreenAutoDimParams.lightThresholdParamType);
        disabled = Parameter(value: arguments[1] as! Bool, type: ScreenAutoDimParams.autoDimDisabledParamType);
    }
}