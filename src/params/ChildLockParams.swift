//
//  ChildLockParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/5/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct ChildLockParams: CommandParams, UIRepresentable
{
	public var lockEnabled: Parameter<Bool>?;
	
	
	public init(lockEnabled: Bool) throws
	{
		self.lockEnabled = Parameter(value: lockEnabled, type: ChildLockParams.enabledParamType);
	}
	
	
	static var enabledParamType: ParameterType
	{
		return .Bool;
	}
	
	
	public init?(_ map: Map)
	{
		mapping(map);
	}
	
	
	public mutating func mapping(map: Map)
	{
		lockEnabled <- (map["child_lock_function"], transformOfParams(type: ChildLockParams.enabledParamType));
	}
	
	
	// MARK: - CommandParams protocol
	
	public static var type: CommandType
	{
		return .ChildLock;
	}
	
	
	public static var paramsList: [ParameterDescription]
	{
		return [ParameterDescription(name: "Lock enabled", type: enabledParamType)];
	}
	
	
	public init(arguments: [Any]) throws
	{
		lockEnabled = Parameter(value: arguments[0] as! Bool, type: ChildLockParams.enabledParamType);
	}
}
