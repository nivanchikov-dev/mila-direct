//
//  SleepModeParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/5/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct SleepModeParams: CommandParams, UIRepresentable
{
	public var enabled: Parameter<Bool>?;
	public var aqi: Parameter<Int>?;
    public var sleepModeLight: Parameter<Int>?;
	public var startHour: Parameter<Int>?;
	public var startMinute: Parameter<Int>?;
	public var endHour: Parameter<Int>?;
	public var endMinute: Parameter<Int>?;
	public var repeatOn: Parameter<Bool>?;
    
	
	
    public init(enabled: Bool, aqi: Int, startHour: Int, startMinute: Int, endHour: Int, endMinute: Int, repeatOn: Bool, light: Int = 5) throws
	{
		self.enabled = Parameter(value: enabled, type: SleepModeParams.enabledParamType);
		self.repeatOn = Parameter(value: repeatOn, type: SleepModeParams.enabledParamType);
		
		self.aqi = Parameter(value: aqi, type: SleepModeParams.aqiParamType);
        self.sleepModeLight = Parameter(value: light, type: SleepModeParams.sleepLightParamType);
		
		self.startHour = Parameter(value: startHour, type: SleepModeParams.hourParamType);
		self.startMinute = Parameter(value: startMinute, type: SleepModeParams.minuteParamType);
		
		self.endHour = Parameter(value: endHour, type: SleepModeParams.hourParamType);
		self.endMinute = Parameter(value: endMinute, type: SleepModeParams.minuteParamType);
	}
	
	
	static var enabledParamType: ParameterType
	{
		return .Bool;
	}
	
	
	static var aqiParamType: ParameterType
	{
		return .Range(min: 0, max: 500);
	}
	
	
	static var hourParamType: ParameterType
	{
		return .Range(min: 0, max: 23);
	}
	
	
	static var minuteParamType: ParameterType
	{
		return .Range(min: 0, max: 59);
	}
	
    
    static var sleepLightParamType: ParameterType
    {
        return .Range(min: 0, max: 255);
    }
    
	
	public init?(_ map: Map)
	{
		mapping(map);
	}
    
    
	
	
	public mutating func mapping(map: Map)
	{
		sleepModeLight <- (map["sleep_mode_light"], transformOfParams(type: SleepModeParams.sleepLightParamType))
		
		enabled <- (map["sleep_mode_function"], transformOfParams(type: SleepModeParams.enabledParamType));
		repeatOn <- (map["sleep_mode_repeat"], transformOfParams(type: SleepModeParams.enabledParamType));
		
		aqi <- (map["sleep_mode_aqi"], transformOfParams(type: SleepModeParams.aqiParamType));
		
		startHour <- (map["sleep_mode_start_hour"], transformOfParams(type: SleepModeParams.hourParamType));
		startMinute <- (map["sleep_mode_start_minute"], transformOfParams(type: SleepModeParams.minuteParamType));
		
		endHour <- (map["sleep_mode_end_hour"], transformOfParams(type: SleepModeParams.hourParamType));
		endMinute <- (map["sleep_mode_end_minute"], transformOfParams(type: SleepModeParams.minuteParamType));
	}
	
	
	// MARK: - CommandParams protocol
	
	public static var type: CommandType
	{
		return .SleepMode;
	}
	
	
	public static var paramsList: [ParameterDescription]
	{
		return [ParameterDescription(name: "Sleep mode enabled", type: enabledParamType),
			ParameterDescription(name: "Max AQI", type: aqiParamType),
            ParameterDescription(name: "Sleep mode light", type: sleepLightParamType),
			ParameterDescription(name: "Start hour", type: hourParamType),
			ParameterDescription(name: "Start minute", type: minuteParamType),
			ParameterDescription(name: "End hour", type: hourParamType),
			ParameterDescription(name: "End minute", type: minuteParamType),
			ParameterDescription(name: "Repeat on", type: enabledParamType)];
	}
	
	
	public init(arguments: [Any]) throws
	{
		enabled = Parameter(value: arguments[0] as! Bool, type: SleepModeParams.enabledParamType);
		
		aqi = Parameter(value: arguments[1] as! Int, type: SleepModeParams.aqiParamType);
        sleepModeLight = Parameter(value: arguments[2] as! Int, type: SleepModeParams.sleepLightParamType);
		
		startHour = Parameter(value: arguments[3] as! Int, type: SleepModeParams.hourParamType);
		startMinute = Parameter(value: arguments[4] as! Int, type: SleepModeParams.minuteParamType);
		
		endHour = Parameter(value: arguments[5] as! Int, type: SleepModeParams.hourParamType);
		endMinute = Parameter(value: arguments[6] as! Int, type: SleepModeParams.minuteParamType);
		
		repeatOn = Parameter(value: arguments[7] as! Bool, type: SleepModeParams.enabledParamType);
	}
}
