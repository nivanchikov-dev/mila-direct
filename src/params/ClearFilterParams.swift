//
//  ClearFilterParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/5/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct ClearFilterParams: CommandParams
{
	private (set) var clearFilter = true;
	
	
	public init()
	{
		
	}
	
	
	public init?(_ map: Map)
	{
		
	}
	
	
	public mutating func mapping(map: Map)
	{
		clearFilter <- map["clear_filter_time_function"];
	}
	
	
	// MARK: - CommandParams protocol
	
	public static var type: CommandType
	{
		return .ClearFilterTime;
	}
}
