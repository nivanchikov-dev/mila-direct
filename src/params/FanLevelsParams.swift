//
//  FanLevelsParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/6/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct FanLevelsParams: CommandParams, UIRepresentable
{
	public var increaseLatency: Parameter<Int>?;
	public var decreaseLatency: Parameter<Int>?;
	
	public var aqiLevels = Array<Parameter<Int>?>(count: 9, repeatedValue: Parameter<Int>.init(value: 1, type: FanLevelsParams.aqiParamType));
	public var fanLevels = Array<Parameter<Int>?>(count: 10, repeatedValue: Parameter<Int>.init(value: 1, type: FanLevelsParams.fanSpeedParamType));
	
	
	public init(aqiLevels: [Int], fanLevels: [Int], increaseLatency: Int, decreaseLatency: Int)
	{
		self.increaseLatency = Parameter(value: increaseLatency, type: FanLevelsParams.secondsParamType);
		self.decreaseLatency = Parameter(value: decreaseLatency, type: FanLevelsParams.secondsParamType);
		
		for (index, value) in aqiLevels.enumerate() {
			self.aqiLevels[index] = Parameter(value: value, type: FanLevelsParams.aqiParamType);
		}
		
		for (index, value) in fanLevels.enumerate() {
			self.fanLevels[index] = Parameter(value: value, type: FanLevelsParams.fanSpeedParamType);
		}
	}
	
	
	
	static var fanSpeedParamType: ParameterType
	{
		return .Range(min: 0x01, max: 0x64);
	}
	
	
	static var secondsParamType: ParameterType
	{
		return .Range(min: 0x01, max: 0xff);
	}
	
	
	static var aqiParamType: ParameterType
	{
		return .Range(min: 0x01, max: 0xFF);
	}
	
	
	public init?(_ map: Map)
	{
		mapping(map);
	}
	
	public mutating func mapping(map: Map)
	{
		increaseLatency <- (map["fan_level_increase_latency"], transformOfParams(type: FanLevelsParams.secondsParamType));
		decreaseLatency <- (map["fan_level_decrease_latency"], transformOfParams(type: FanLevelsParams.secondsParamType));
		
		aqiLevels[0] <- (map["fan_level_aqi_1"], transformOfParams(type: FanLevelsParams.aqiParamType));
		aqiLevels[1] <- (map["fan_level_aqi_2"], transformOfParams(type: FanLevelsParams.aqiParamType));
		aqiLevels[2] <- (map["fan_level_aqi_3"], transformOfParams(type: FanLevelsParams.aqiParamType));
		aqiLevels[3] <- (map["fan_level_aqi_4"], transformOfParams(type: FanLevelsParams.aqiParamType));
		aqiLevels[4] <- (map["fan_level_aqi_5"], transformOfParams(type: FanLevelsParams.aqiParamType));
		aqiLevels[5] <- (map["fan_level_aqi_6"], transformOfParams(type: FanLevelsParams.aqiParamType));
		aqiLevels[6] <- (map["fan_level_aqi_7"], transformOfParams(type: FanLevelsParams.aqiParamType));
		aqiLevels[7] <- (map["fan_level_aqi_8"], transformOfParams(type: FanLevelsParams.aqiParamType));
		aqiLevels[8] <- (map["fan_level_aqi_9"], transformOfParams(type: FanLevelsParams.aqiParamType));
		
		fanLevels[0] <- (map["fan_level_fan_speed_1"], transformOfParams(type: FanLevelsParams.fanSpeedParamType));
		fanLevels[1] <- (map["fan_level_fan_speed_2"], transformOfParams(type: FanLevelsParams.fanSpeedParamType));
		fanLevels[2] <- (map["fan_level_fan_speed_3"], transformOfParams(type: FanLevelsParams.fanSpeedParamType));
		fanLevels[3] <- (map["fan_level_fan_speed_4"], transformOfParams(type: FanLevelsParams.fanSpeedParamType));
		fanLevels[4] <- (map["fan_level_fan_speed_5"], transformOfParams(type: FanLevelsParams.fanSpeedParamType));
		fanLevels[5] <- (map["fan_level_fan_speed_6"], transformOfParams(type: FanLevelsParams.fanSpeedParamType));
		fanLevels[6] <- (map["fan_level_fan_speed_7"], transformOfParams(type: FanLevelsParams.fanSpeedParamType));
		fanLevels[7] <- (map["fan_level_fan_speed_8"], transformOfParams(type: FanLevelsParams.fanSpeedParamType));
		fanLevels[8] <- (map["fan_level_fan_speed_9"], transformOfParams(type: FanLevelsParams.fanSpeedParamType));
		fanLevels[9] <- (map["fan_level_fan_speed_10"], transformOfParams(type: FanLevelsParams.fanSpeedParamType));
	}
	
	
	public static var type: CommandType
	{
		return .FanLevels;
	}
	
	
	public static var paramsList: [ParameterDescription]
	{
		var list = [ParameterDescription]();
		
		list.append(ParameterDescription(name: "Increase latency", type: secondsParamType));
		list.append(ParameterDescription(name: "Decrease latency", type: secondsParamType));
		
		for i in 1 ... 9 {
			list.append(ParameterDescription(name: "AQI level \(i)", type: aqiParamType));
		}
		
		for i in 1 ... 10 {
			list.append(ParameterDescription(name: "Fan level \(i)", type: fanSpeedParamType));
		}
		
		return list;
	}
	
	
	public init(arguments: [Any]) throws
	{
		increaseLatency = Parameter(value: arguments[0] as! Int, type: FanLevelsParams.secondsParamType);
		decreaseLatency = Parameter(value: arguments[1] as! Int, type: FanLevelsParams.secondsParamType);
	
		var offset = 2;
		
		var range = offset;
		
		for i in range ..< (range + 9) {
			self.aqiLevels[i - range] = Parameter(value: arguments[i] as! Int, type: FanLevelsParams.aqiParamType);
			offset += 1;
		}
		
		range = offset;
		
		for i in range ..< (range + 10) {
			self.fanLevels[i - range] = Parameter(value: arguments[i] as! Int, type: FanLevelsParams.fanSpeedParamType);
		}
	}
}
