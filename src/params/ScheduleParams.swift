//
//  ScheduleParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/6/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct ScheduleParams: CommandParams, UIRepresentable
{
	public var enabled: Parameter<Bool>?;
	public var autoModeEnabled: Parameter<Bool>?;
	public var repeatOn: Parameter<Bool>?;
	public var fanSpeed: Parameter<Int>?;
	public var hour: Parameter<Int>?;
	public var minute: Parameter<Int>?;
	
	
	public init(enabled: Bool, autoMode: Bool, repeatOn: Bool, fanSpeed: Int, hour: Int, minute: Int) throws
	{
		self.enabled = Parameter(value: enabled, type: ScheduleParams.enabledParamType);
		self.autoModeEnabled = Parameter(value: autoMode, type: ScheduleParams.enabledParamType);
		self.repeatOn = Parameter(value: repeatOn, type: ScheduleParams.enabledParamType);
		
		self.fanSpeed = Parameter(value: fanSpeed, type: ScheduleParams.fanSpeedParamType);
		
		self.hour = Parameter(value: hour, type: ScheduleParams.hourParamType);
		self.minute = Parameter(value: minute, type: ScheduleParams.minuteParamType);
	}
	
	
	
	static var enabledParamType: ParameterType
	{
		return .Bool;
	}
	
	
	static var hourParamType: ParameterType
	{
		return .Range(min: 0, max: 23);
	}
	
	
	static var minuteParamType: ParameterType
	{
		return .Range(min: 0, max: 59);
	}
	
	static var fanSpeedParamType: ParameterType
	{
		return .Range(min: 0x01, max: 0x64);
	}
	
	
	
	public init?(_ map: Map)
	{
		mapping(map);
	}
	
	public mutating func mapping(map: Map)
	{
		// misspelled 'schedule_funCtion'
		enabled <- (map["schedule_funtion"], transformOfParams(type: ScheduleParams.enabledParamType));
		
		hour <- (map["schedule_hour"], transformOfParams(type: ScheduleParams.hourParamType));
		minute <- (map["schedule_minute"], transformOfParams(type: ScheduleParams.minuteParamType));
		
		repeatOn <- (map["schedule_repeat"], transformOfParams(type: ScheduleParams.enabledParamType));
		autoModeEnabled <- (map["schedule_work_mode"], transformOfParams(type: ScheduleParams.enabledParamType));
		
		fanSpeed <- (map["schedule_work_fan_speed"], transformOfParams(type: ScheduleParams.fanSpeedParamType));
	}
	
	
	public static var type: CommandType
	{
		return .Schedule;
	}
	
	
	public static var paramsList: [ParameterDescription]
	{
		return [ParameterDescription(name: "Schedule enabled", type: enabledParamType),
                ParameterDescription(name: "Auto Mode enabled", type: enabledParamType),
				ParameterDescription(name: "Repeat enabled", type: enabledParamType),
				ParameterDescription(name: "Fan Speed", type: fanSpeedParamType),
                ParameterDescription(name: "Start hour", type: hourParamType),
                ParameterDescription(name: "Start minute", type: minuteParamType),];
	}
	
	
	public init(arguments: [Any]) throws
	{
		self.enabled = Parameter(value: arguments[0] as! Bool, type: ScheduleParams.enabledParamType);
        self.autoModeEnabled = Parameter(value: arguments[1] as! Bool, type: ScheduleParams.enabledParamType);

		self.repeatOn = Parameter(value: arguments[2] as! Bool, type: ScheduleParams.enabledParamType);
        self.fanSpeed = Parameter(value: arguments[3] as! Int, type: ScheduleParams.fanSpeedParamType);
		self.hour = Parameter(value: arguments[4] as! Int, type: ScheduleParams.hourParamType);
		self.minute = Parameter(value: arguments[5] as! Int, type: ScheduleParams.minuteParamType);
		
		
		
				
		
	}
}
