//
//  DeviceNumberParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/5/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct DeviceNumberParams: CommandParams, UIRepresentable
{
	public var number: Parameter<Int>?;
	var setNumber: Int = 2;
	
	
	public init(number: Int) throws
	{
		self.number = Parameter(value: number, type: DeviceNumberParams.numberParamType);
	}
	
	
	static var numberParamType: ParameterType
	{
		return .Range(min: 0x01, max: 0xFF);
	}
	
	
	public init?(_ map: Map)
	{
		
	}
	
	
	public mutating func mapping(map: Map)
	{
		number <- (map["device_number_value"], transformOfParams(type: DeviceNumberParams.numberParamType));
		setNumber <- map["device_number_function"]
	}
	
	
	// MARK: - CommandParams protocol
	
	public static var type: CommandType
	{
		return .DeviceNumber;
	}
	
	
	public static var paramsList: [ParameterDescription]
	{
		return [ParameterDescription(name: "Device number", type: numberParamType)];
	}
	
	
	public init(arguments: [Any]) throws
	{
		number = Parameter(value: arguments[0] as! Int, type: DeviceNumberParams.numberParamType);
	}
}
