//
//  SynchroniseTimeParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/6/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper
import Foundation


public struct SynchroniseTimeParams: CommandParams
{
	public var time: Double = NSDate().timeIntervalSince1970;;
	
	public init()
	{

	}
	
	
	public init?(_ map: Map)
	{
		
	}
	
	
	public mutating func mapping(map: Map)
	{
		time <- (map["current_utc_time"], transformOfTimeStamp);
	}
	
	
	public static var type: CommandType
	{
		return .SynchroniseTime;
	}
	
	
	var transformOfTimeStamp: TransformOf<Double, NSNumber>
	{
		return TransformOf<Double, NSNumber>(fromJSON: { (number) -> Double? in
				return number?.doubleValue;
			}, toJSON: { (value) -> NSNumber? in
				return value.map { NSNumber(double: floor($0)) } ?? nil;
		})
	}
	
	
	public static var paramsList: [ParameterDescription]
	{
		return [ParameterDescription(name: "Current timestamp", type: .Time)];
	}
}
