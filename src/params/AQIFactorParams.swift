//
//  AQIFactorParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/6/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct AQIFactorParams: CommandParams, UIRepresentable
{
	public var aqiFactor: Parameter<Int>?;
	var kFactor = 0;
	var bFactor = 0;
	
	
	public init(aqiFactor: Int = 101) throws
	{
		self.aqiFactor = Parameter(value: aqiFactor, type: AQIFactorParams.aqiFactorParamType);
	}
	
	
	static var aqiFactorParamType: ParameterType
	{
		return .Range(min: 50, max: 150)
	}
	
	
	public init?(_ map: Map)
	{
		
	}
	
	mutating public func mapping(map: Map)
	{
		aqiFactor <- (map["aqi_factor_x_setting"], transformOfParams(type: AQIFactorParams.aqiFactorParamType));
		kFactor <- map["aqi_factor_k"];
		bFactor <- map["aqi_factor_b"];
	}
	
	
	public static var type: CommandType
	{
		return .AQIxFactor;
	}
	
	
	public static var paramsList: [ParameterDescription]
	{
		return [ParameterDescription(name: "AQI X Factor", type: aqiFactorParamType)];
	}
	
	
	public init(arguments: [Any]) throws
	{
		aqiFactor = Parameter(value: arguments[0] as! Int, type: AQIFactorParams.aqiFactorParamType);
	}
}
