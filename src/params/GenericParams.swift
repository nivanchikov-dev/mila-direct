//
//  GenericParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/5/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct GenericParams: CommandParams
{
	public init () { }
	
	public init?(_ map: Map)
	{
		
	}
	
	public mutating func mapping(map: Map)
	{
		
	}
	
	public static var type: CommandType
	{
		return .Unknown;
	}
}
