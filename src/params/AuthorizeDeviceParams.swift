//
//  AuthorizeDeviceParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/6/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct AuthorizeDeviceParams: CommandParams, UIRepresentable
{
	public var enabled: Parameter<Bool>?;
	public var month: Parameter<Int>?;
	public var day: Parameter<Int>?;
	public var hour: Parameter<Int>?;
	public var powerDownMode: Int?;
	public var status: Int?;
	
	
	public init(enabled: Bool, month: Int, day: Int, hour: Int)
	{
		self.enabled = Parameter(value: enabled, type: AuthorizeDeviceParams.enabledParamType);
		self.month = Parameter(value: month, type: AuthorizeDeviceParams.monthParamType);
		self.day = Parameter(value: day, type: AuthorizeDeviceParams.dayParamType);
		self.hour = Parameter(value: hour, type: AuthorizeDeviceParams.hourParamType);
	}
	
	
	static var enabledParamType: ParameterType
	{
		return .Bool;
	}
	
	
	static var monthParamType: ParameterType
	{
		return .Range(min: 0, max: 100);
	}
	
	
	static var dayParamType: ParameterType
	{
		return .Range(min: 0, max: 31);
	}
	
	
	static var hourParamType: ParameterType
	{
		return .Range(min: 0, max: 23);
	}
	
	
	
	public init?(_ map: Map)
	{
		mapping(map);
	}
	
	public mutating func mapping(map: Map)
	{
		enabled <- (map["authorize_device_function"], transformOfParams(type: AuthorizeDeviceParams.enabledParamType));
		month <- (map["authorize_device_month"], transformOfParams(type: AuthorizeDeviceParams.monthParamType));
		day <- (map["authorize_device_day"], transformOfParams(type: AuthorizeDeviceParams.dayParamType));
		hour <- (map["authorize_device_hour"], transformOfParams(type: AuthorizeDeviceParams.hourParamType));
		powerDownMode <- map["auth_out_power_down_mode"];
		status <- map["authorization_status"]
	}
	
	public static var type: CommandType
	{
		return .AuthorizeDevice;
	}
	
	
	public static var paramsList: [ParameterDescription]
	{
		return [ParameterDescription(name: "Enable authorization", type: enabledParamType),
				ParameterDescription(name: "Auth month", type: monthParamType),
				ParameterDescription(name: "Auth day", type: dayParamType),
				ParameterDescription(name: "Auth hour", type: hourParamType)];
	}
	
	
	public init(arguments: [Any]) throws
	{
		enabled = Parameter(value: arguments[0] as! Bool, type: AuthorizeDeviceParams.enabledParamType);
		month = Parameter(value: arguments[1] as! Int, type: AuthorizeDeviceParams.monthParamType);
		day = Parameter(value: arguments[2] as! Int, type: AuthorizeDeviceParams.dayParamType);
		hour = Parameter(value: arguments[3] as! Int, type: AuthorizeDeviceParams.hourParamType);
	}
}
