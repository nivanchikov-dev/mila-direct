//
//  PowerModeParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/4/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public enum WorkingMode: Int
{
	case Manual = 1;
	case Auto = 2;
	case Monitor = 4;
	case Standby = 8;
	case Purification = 16;
	
	static let allValues = [.Manual, Auto, .Monitor, .Standby, .Purification];
    
    public var isOn: Bool {
        return (self == .Auto) || (self == .Manual) || (self == .Purification);
    }
}


public typealias ParameterDescription = (name: String, type: ParameterType);


public struct PowerModeParams: CommandParams, UIRepresentable
{
	public var workingMode: Parameter<WorkingMode>?;
	public var fanSpeed: Parameter<Int>?;
	
	
	public init(mode: WorkingMode, fanSpeed: Int = 0x01) throws
	{
		if (fanSpeed < 0x01 || fanSpeed > 0x64) {
			throw NSError(domain: "com.mila.debugger", code: 0, userInfo: nil);
		}
		
		self.fanSpeed = Parameter<Int>(value: fanSpeed, type: PowerModeParams.fanSpeedParamType);
		self.workingMode = Parameter<WorkingMode>(value: mode, type: PowerModeParams.workingModeParamType);
	}
	
	
	static var fanSpeedParamType: ParameterType
	{
		return .Range(min: 0x01, max: 0x64)
	}
	
	
	static var workingModeParamType: ParameterType
	{
		return .Values(values: WorkingMode.allValues.map { $0.rawValue });
	}
	
	
	public init?(_ map: Map)
	{
		
	}
	
	
	public mutating func mapping(map: Map)
	{
		workingMode <- (map["working_mode"], transformOfParams(type: PowerModeParams.workingModeParamType));
		fanSpeed <- (map["fan_speed"], transformOfParams(type: PowerModeParams.fanSpeedParamType));
	}
	
	
	// MARK: - CommandParams protocol
	
	public static var type: CommandType
	{
		return .PowerMode;
	}
	
	public static var paramsList: [ParameterDescription]
	{
		return [ParameterDescription(name: "Working mode", type: workingModeParamType),
			ParameterDescription(name: "Fan speed", type: fanSpeedParamType)];
	}
	
	
	public init(arguments: [Any]) throws
	{
		self.workingMode = Parameter(value: WorkingMode(rawValue: arguments[0] as! Int)!, type: PowerModeParams.workingModeParamType);
		self.fanSpeed = Parameter(value: arguments[1] as! Int, type: PowerModeParams.fanSpeedParamType);
	}
}


extension CommandParams
{
	func transformOfParams<T>(type type: ParameterType) -> TransformOf<Parameter<T>, Any>
	{
		return TransformOf<Parameter<T>, Any>(fromJSON: { (value) -> Parameter<T>? in
			
			if let res = value as? T {
				return Parameter(value: res, type: type);
			}
			
			return nil;
		},
		toJSON: { (parameter) -> Any? in
			return parameter?.value;
		});
	}
	
	
	func transformOfParams<T: RawRepresentable>(type type: ParameterType) -> TransformOf<Parameter<T>, Any>
	{
		return TransformOf<Parameter<T>, Any>(fromJSON: { (value) -> Parameter<T>? in
			
				if let rawValue = value as? T.RawValue, res = T(rawValue: rawValue)  {
					return Parameter(value: res, type: type);
				}
			
				return nil;
			},
			toJSON: { (parameter) -> Any? in
				return parameter?.value.rawValue;
			});
	}
}
