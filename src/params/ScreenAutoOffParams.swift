//
//  ScreenAutoOffParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/5/15.
//  Copyright © 2015 Mila. All rights reserved.
//

import ObjectMapper


public struct ScreenAutoOffParams: CommandParams, UIRepresentable
{
	public var secondsForAutoOff: Parameter<Int>?;
	public var disabled: Parameter<Bool>?;
	
	
	public init(autoOffDisabled: Bool, seconds: Int = 0x01) throws
	{
		self.disabled = Parameter(value: autoOffDisabled, type: ScreenAutoOffParams.autoOffDisabledParamType);
		self.secondsForAutoOff = Parameter(value: seconds, type: ScreenAutoOffParams.secondsForAutoOffParamType);
	}
	
	
	static var autoOffDisabledParamType: ParameterType
	{
		return .Bool;
	}
	
	
	static var secondsForAutoOffParamType: ParameterType
	{
		return .Range(min: 0x01, max: 0xFF);
	}
	
	
	public init?(_ map: Map)
	{
		mapping(map);
	}
	
	
	public mutating func mapping(map: Map)
	{
		disabled <- (map["screen_timer_off_function"], transformOfParams(type: ScreenAutoOffParams.autoOffDisabledParamType));
		secondsForAutoOff <- (map["screen_timer_off_seconds"], transformOfParams(type: ScreenAutoOffParams.secondsForAutoOffParamType));
	}
	
	
	// MARK: - CommandParams protocol
	
	public static var type: CommandType
	{
		return .ScreenAutoOff;
	}
	
	public static var paramsList: [ParameterDescription]
	{
		return [ParameterDescription(name: "Seconds before auto-off", type: secondsForAutoOffParamType),
                ParameterDescription(name: "Screen always on", type: autoOffDisabledParamType),];
	}

	
	public init(arguments: [Any]) throws
	{
        secondsForAutoOff = Parameter(value: arguments[0] as! Int, type: ScreenAutoOffParams.secondsForAutoOffParamType);
		disabled = Parameter(value: arguments[1] as! Bool, type: ScreenAutoOffParams.autoOffDisabledParamType);
	}
}
