//
//  UpdateFirmwareParams.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/6/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import ObjectMapper


public struct UpdateFirmwareParams: CommandParams
{
	var directory = "http://be2.mymila.co/device/firmware/";
	var filename = "lpb100_upgrade.bin";
	
	
	public init ()
	{
		
	}
	
	
	public init?(_ map: Map)
	{
		
	}
	
	public mutating func mapping(map: Map)
	{
		directory <- map["url_dir"];
		filename <- map["file_name"];
	}
	
	public static var type: CommandType
	{
		return .UpdateDeviceFirmware;
	}
}
