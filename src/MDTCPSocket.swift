//
//  MDTCPSocket.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/16/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import CocoaAsyncSocket
import CocoaLumberjack


protocol MDTCPSocketListener: class
{
	func socket(sock: MDTCPSocket, didConnectToHost host: String!, port: UInt16);
	
	func socket(sock: MDTCPSocket, didWriteDataWithTag tag: Int);
	
	func socket(sock: MDTCPSocket, didReadData data: NSData!, withTag tag: Int)
	
	func socketDidDisconnect(sock: MDTCPSocket, withError err: NSError!)
}


extension MDTCPSocketListener
{
	func socket(sock: MDTCPSocket, didConnectToHost host: String!, port: UInt16) {}
	
	func socket(sock: MDTCPSocket, didWriteDataWithTag tag: Int) {}
	
	func socket(sock: MDTCPSocket, didReadData data: NSData!, withTag tag: Int) {}
	
	func socketDidDisconnect(sock: MDTCPSocket, withError err: NSError!) {}
}


class MDTCPSocket: GCDAsyncSocket
{
	private lazy var listeners = NSHashTable.weakObjectsHashTable()
	
	private (set) var ipAddress: String;
	private (set) var deviceUID: String;
	
	private var queue: dispatch_queue_t;
	
	var logLevel: DDLogLevel;
	
	
	init(deviceUID: String, ipAddress: String, delegateQueue: dispatch_queue_t, logLevel: DDLogLevel = DDLogLevel.Verbose)
	{
		self.deviceUID = deviceUID;
		self.ipAddress = ipAddress;
		
		self.queue = dispatch_queue_create((ipAddress as NSString).UTF8String, DISPATCH_QUEUE_CONCURRENT);
		self.logLevel = logLevel;
		
		super.init(delegate: nil, delegateQueue: delegateQueue, socketQueue: nil);
		
		synchronouslySetDelegate(self);
	}
	
	
	func updateWithDevice(deviceUID: String, ipAddress: String)
	{
		self.deviceUID = deviceUID;
		self.ipAddress = ipAddress;
	}
	
	
	func connect() throws
	{
		try connectToHost(ipAddress, onPort: 8899, withTimeout: 10.0);
	}
	
	
	override func disconnect()
	{
		synchronouslySetDelegate(nil);
		super.disconnect();
	}
	
	
	deinit
	{
		disconnect();
	}
	
	
	func addListener(listener: MDTCPSocketListener)
	{
		dispatch_barrier_async(queue) { [weak self] () -> Void in
			self?.listeners.addObject(listener);
		}
	}
	
	
	func removeListener(listener: MDTCPSocketListener)
	{
		dispatch_barrier_async(queue) { [weak self] () -> Void in
			self?.listeners.removeObject(listener);
		}
	}
}


extension MDTCPSocket: GCDAsyncSocketDelegate
{
	@objc internal func socket(sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16)
	{
		DDLogDebug("TCP socket for \(deviceUID) connected to \(host)", level: logLevel);
		
		var cListeners: [AnyObject]?
		
		dispatch_barrier_sync(queue) {
			cListeners = self.listeners.allObjects;
		}
		
		cListeners?.forEach { ($0 as? MDTCPSocketListener)?.socket(self, didConnectToHost: host, port: port) };
		
		sock.readDataToData(GCDAsyncSocket.ZeroData(), withTimeout: -1.0, tag: 1);
	}
	
	
	@objc internal func socket(sock: GCDAsyncSocket, didWriteDataWithTag tag: Int)
	{
		DDLogDebug("TCP socket for \(deviceUID)@\(sock.connectedHost) wrote data", level: logLevel);
		
		var cListeners: [AnyObject]?
		
		dispatch_barrier_sync(queue) {
			cListeners = self.listeners.allObjects;
		}
		
		cListeners?.forEach { ($0 as? MDTCPSocketListener)?.socket(self, didWriteDataWithTag: tag) };
	}
	
	
	@objc internal func socket(sock: GCDAsyncSocket, didReadData data: NSData, withTag tag: Int)
	{
		DDLogDebug("TCP socket for \(deviceUID)@\(sock.connectedHost) read data", level: logLevel);
		
		var cleanData: NSData!;
		
		autoreleasepool ({
			let mutableData = NSMutableData(data: data);
			mutableData.length = mutableData.length - GCDAsyncSocket.ZeroData().length;
			cleanData = NSData(data: mutableData);
		})
		
		var cListeners: [AnyObject]?
		
		dispatch_barrier_sync(queue) {
			cListeners = self.listeners.allObjects;
		}
		
		cListeners?.forEach { ($0 as? MDTCPSocketListener)?.socket(self, didReadData: cleanData, withTag: tag) };
		
		sock.readDataToData(GCDAsyncSocket.ZeroData(), withTimeout: -1.0, tag: tag)
	}
	
	
	@objc internal func socketDidDisconnect(sock: GCDAsyncSocket, withError err: NSError?)
	{
		DDLogError("TCP socket for \(deviceUID) disconnected due to \(err)", level: logLevel);
		
		var cListeners: [AnyObject]?
		
		dispatch_barrier_sync(queue) {
			cListeners = self.listeners.allObjects;
		}
		
		cListeners?.forEach { ($0 as? MDTCPSocketListener)?.socketDidDisconnect(self, withError: err) };
	}
}
