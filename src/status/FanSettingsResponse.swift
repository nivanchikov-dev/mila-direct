//
//  FanSettingsResponse.swift
//  mila-direct
//
//  Created by Nikita Ivanchikov on 12/1/15.
//  Copyright © 2015 Nikita Ivanchikov. All rights reserved.
//

import ObjectMapper


public struct FanSettingsResponse: CommandParams
{
	public private (set) var currentFanSpeed: Int?;
	public private (set) var manualFanSpeed: Int?;
	public private (set) var vocFanLevel: Int?;
	public private (set) var vocFanSpeed: Int?;
	public private (set) var aqiFanLevel: Int?;
	public private (set) var aqiFanSpeed: Int?;
	public private (set) var fanLevels: FanLevelsParams?;
    public private (set) var time: NSTimeInterval?;
	
	
	public init?(_ map: Map)
	{
		
	}
	
	
	public mutating func mapping(map: Map)
	{
		fanLevels = FanLevelsParams(map);
		currentFanSpeed <- map["current_fan_speed"];
		manualFanSpeed <- map["fan_speed"];
		vocFanLevel <- map["voc_fan_level"];
		vocFanSpeed <- map["voc_fan_speed"];
		aqiFanLevel <- map["aqi_fan_level"];
		aqiFanSpeed <- map["aqi_fan_speed"];
        time <- map["current_utc_time"]
	}
	
	public static var type: CommandType
	{
		return .FanSettings;
	}
}
