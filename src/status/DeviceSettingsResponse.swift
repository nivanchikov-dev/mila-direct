//
//  DeviceSettingsResponse.swift
//  mila-direct
//
//  Created by Nikita Ivanchikov on 12/1/15.
//  Copyright © 2015 Nikita Ivanchikov. All rights reserved.
//

import ObjectMapper


public struct DeviceSettingsResponse: CommandParams
{
	public var deviceAuth: AuthorizeDeviceParams?;
	public var schedule: ScheduleParams?;
	public var sleepMode: SleepModeParams?;
	public var screenAutoOff: ScreenAutoOffParams?;
    public var screenAutoDim: ScreenAutoDimParams?;
	public var childLock: ChildLockParams?;
	
	public var error: Int?;
	public var filterUsageTime: Int?;
	public var currentLight: Int?;
	public var dayNightLightThreshold: Int?;
	public var time: NSTimeInterval?;
	public var workingMode: Int?;

	
	public init?(_ map: Map)
	{
		
	}
	
	
	public mutating func mapping(map: Map)
	{
		deviceAuth = AuthorizeDeviceParams(map);
		schedule = ScheduleParams(map);
		sleepMode = SleepModeParams(map);
		screenAutoOff = ScreenAutoOffParams(map);
		childLock = ChildLockParams(map);
        screenAutoDim = ScreenAutoDimParams(map);
		
		error <- map["error_number"]
		filterUsageTime  <- map["filter_usage_time"]
		currentLight  <- map["current_light"]
		dayNightLightThreshold  <- map["current_light_day_night_threshold"]
		time  <- map["current_utc_time"]
		workingMode <- (map["working_mode"], workingModeTransform);
	}
	
	
	public static var type: CommandType
	{
		return .DeviceSettings;
	}
	
	
	private var workingModeTransform: TransformOf<Int, NSNumber>
	{
		return TransformOf<Int, NSNumber>(fromJSON: { (number) -> Int? in
			
				// since we get values from 0 to 4, we need to map them to protocol working modes of 1 / 2 / 4 / 8 / 16 using powers of 2
				return number.map { 1 << $0.integerValue } ?? nil;
			
			}, toJSON: { (value) -> NSNumber? in
				return value.map{ NSNumber(integer: $0) } ?? nil;
			})
	}
}



