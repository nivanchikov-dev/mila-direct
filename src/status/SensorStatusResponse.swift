//
//  SensorStatusResponse.swift
//  mila-direct
//
//  Created by Nikita Ivanchikov on 12/1/15.
//  Copyright © 2015 Nikita Ivanchikov. All rights reserved.
//


import ObjectMapper


public struct SensorStatusResponse: CommandParams
{
	public var voc: Int?;
	public var temperature: Int?;
	public var humidity: Int?;
	public var aqi: Int?;
    public var p1: Int?;
    public var pm25: Int?;
    public var p2: Int?;
	public var time: NSTimeInterval?;
	

	public init?(_ map: Map)
	{
		
	}
	
	
	public mutating func mapping(map: Map)
	{
		voc <- map["current_voc"]
		temperature <- map["current_temperate"]
		humidity <- map["current_humidity"]
		aqi <- map["current_aqi"]
        p1 <- map["current_p1"]
        pm25 <- map["current_pm2.5", nested: false]
        p2 <- map["current_p2"]
		time <- map["current_utc_time"]
	}
	
	
	public static var type: CommandType
	{
		return .SensorStatus;
	}
}