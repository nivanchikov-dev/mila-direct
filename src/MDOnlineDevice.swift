//
//  MDOnlineDevice.swift
//  mila-direct
//
//  Created by Nikita Ivanchikov on 11/24/15.
//  Copyright © 2015 Nikita Ivanchikov. All rights reserved.
//


public struct MDOnlineDevice: Hashable
{
	public let ip: String;
	public let mac: String;
	public let updated: NSTimeInterval;
	
	
	init(ip: String, mac: String)
	{
		self.ip = ip;
		self.mac = mac;
		
		updated = NSDate().timeIntervalSince1970;
	}
	
	
	public var hashValue: Int
	{
		return mac.hashValue ^ ip.hashValue;
	}
}


public func ==(lhs: MDOnlineDevice, rhs: MDOnlineDevice) -> Bool
{
	return (lhs.mac == rhs.mac) && (lhs.ip == rhs.ip);
}
