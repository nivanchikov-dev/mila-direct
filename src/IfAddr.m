//
//  IfAddr.m
//  mila-direct
//
//  Created by Nikita Ivanchikov on 11/24/15.
//  Copyright © 2015 Nikita Ivanchikov. All rights reserved.
//

#import "IfAddr.h"
#import <ifaddrs.h>
#import <arpa/inet.h>
#import <net/if.h>
#import <netdb.h>


@implementation IfAddr


+ (nullable NSString*) networkMask
{
	struct ifaddrs *ifaddr = NULL;
	
	if (getifaddrs(&ifaddr) != 0) {
		return nil;
	}
	
	struct ifaddrs* ptr = ifaddr;
	
	NSString* address = nil;
	
	while (ptr != NULL)
	{
		NSString* name = [[NSString alloc] initWithUTF8String: ptr->ifa_name];
		
		if ([name caseInsensitiveCompare: @"en0"] == NSOrderedSame)
		{
			unsigned int flags = ptr->ifa_flags;
			
			if ((flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING))
			{
				if (ptr->ifa_addr->sa_family == AF_INET)
				{
					struct sockaddr	*addr  = ptr->ifa_addr;
					
					char hostname[NI_MAXHOST];
					
					if (getnameinfo(addr, sizeof(addr->sa_len), hostname, sizeof(hostname), nil, sizeof(0), NI_NUMERICHOST) == 0)
					{
						address = [[NSString alloc] initWithCString: hostname encoding: NSUTF8StringEncoding];
						break;
					}
				}
			}
		}
		
		ptr = ptr->ifa_next;
	}
	
	free(ifaddr);
	
	address = address.stringByDeletingPathExtension;
	
	return address;
}


@end
