//
//  MDUDPSocket.swift
//  mila-debugger
//
//  Created by Nikita Ivanchikov on 11/6/15.
//  Copyright © 2015 Mila. All rights reserved.
//


import CocoaAsyncSocket
import CocoaLumberjack


protocol MDUDPSocketDelegate: class
{
    func udpSocket(socket: MDUDPSocket, heartbeatFailedDueTo error: ErrorType);
    func udpSocketSentHeartbeatMessage(socket: MDUDPSocket);
    func udpSocket(socket: MDUDPSocket, discoveredOnlineDevice device: MDOnlineDevice);
}


enum HearbeatSocketState
{
    case Idle
    case Closed
    case Running
    case Error
}


final class MDUDPSocket: AnyObject, GCDAsyncUdpSocketDelegate
{
    private (set) var socketState: HearbeatSocketState = .Idle;
    
    internal weak var delegate: MDUDPSocketDelegate? = nil;
    
    private var socket: GCDAsyncUdpSocket;
    private var password: String;
    private var port: Int;
    private (set) var heartbeatInterval: NSTimeInterval;
    private let delegateQueue = dispatch_queue_create("com.mila.debugger.udpDelegateQueue", DISPATCH_QUEUE_CONCURRENT);
    
    var logLevel: DDLogLevel;
    
    
    private var heartbeatTimer: NSTimer?
        {
        willSet {
            if let t = heartbeatTimer where t.valid {
                t.invalidate();
            }
        }
    }
    
    
    // MARK: - Lifecycle
    
    init(password: String, port: Int, heartbeatInterval: NSTimeInterval, logLevel: DDLogLevel = DDLogLevel.Verbose) throws
    {
        self.password = password;
        self.port = port;
        self.heartbeatInterval = heartbeatInterval;
        self.logLevel = logLevel;
        
        socket = GCDAsyncUdpSocket();
        
        if password.isEmpty {
            DDLogError("UDP password is empty", level: logLevel);
            throw MilaDirectError.InvalidUDPPassword;
        }
        
        socket.synchronouslySetDelegate(self, delegateQueue: delegateQueue);
        
        try socket.enableBroadcast(true);
        try socket.beginReceiving();
    }
    
    
    deinit
    {
        stopHeartbeat();
    }
    
    
    func startHeartbeat() throws
    {
        if (socketState == .Running) {
            return;
        }
        
        if (socketState == .Closed) {
            try socket.enableBroadcast(true);
            try socket.beginReceiving();
        }
        
        try sendHeartbeatMessage();
    }
    
    
    func stopHeartbeat()
    {
        socketState = .Idle;
        heartbeatTimer = nil;
    }
    
    
    private func sendHeartbeatMessage() throws
    {
        stopHeartbeat();
        
        guard let messageData = password.dataUsingEncoding(NSUTF8StringEncoding) else
        {
            DDLogError("Failed to convert UDP password \(password) to NSData", level: logLevel);
            socketState = .Error;
            throw MilaDirectError.InvalidUDPPassword;
        }
        
        if (socketState != .Running) {
            socketState = .Running;
        }
        
        let host = (IfAddr.networkMask() ?? "255.255.255").stringByAppendingString(".255");
        
        DDLogDebug("UDP socket detected current network mask \(host)", level: logLevel);
        
        socket.sendData(messageData, toHost: host, port: UInt16(port), withTimeout: 10.0, tag: 1);
    }
    
    
    private func launchTimer()
    {
        heartbeatTimer = nil;
        
        let runloop = NSRunLoop.currentRunLoop();
        
        heartbeatTimer = NSTimer.scheduledTimerWithTimeInterval(heartbeatInterval, target: self, selector: #selector(MDUDPSocket.onTimer), userInfo: nil, repeats: false);
        
        runloop.addTimer(heartbeatTimer!, forMode: NSDefaultRunLoopMode);
        
        let updateInterval = 0.1;
        
        var date = NSDate(timeIntervalSinceNow: updateInterval);
        
        while ((heartbeatTimer?.valid ?? false) && runloop.runMode(NSDefaultRunLoopMode, beforeDate: date)) {
            date = NSDate(timeIntervalSinceNow: updateInterval);
        }
    }
    
    
    @objc private func onTimer()
    {
        do {
            try sendHeartbeatMessage();
        }
        catch (let error)
        {
            DDLogError("UDP heartbeat message failed due to \(error)", level: logLevel);
            
            stopHeartbeat();
            
            socketState = .Error;
            delegate?.udpSocket(self, heartbeatFailedDueTo: error);
        }
    }
}


extension MDUDPSocket
{
    @objc internal func udpSocket(sock: GCDAsyncUdpSocket, didSendDataWithTag tag: Int)
    {
        DDLogDebug("UDP socket sent broadcast data", level: logLevel);
        
        delegate?.udpSocketSentHeartbeatMessage(self);
        launchTimer();
    }
    
    
    @objc internal func udpSocket(sock: GCDAsyncUdpSocket, didNotSendDataWithTag tag: Int, dueToError error: NSError)
    {
        DDLogError("UDP heartbeat message failed due to \(error)", level: logLevel);
        
        socketState = .Error;
        delegate?.udpSocket(self, heartbeatFailedDueTo: error);
    }
    
    
    @objc internal func udpSocketDidClose(sock: GCDAsyncUdpSocket, withError error: NSError)
    {
        DDLogError("UDP heartbeat message failed due to \(error)", level: logLevel);
        
        socketState = .Closed;
        delegate?.udpSocket(self, heartbeatFailedDueTo: error);
    }
    
    
    @objc internal func udpSocket(sock: GCDAsyncUdpSocket, didReceiveData data: NSData, fromAddress address: NSData, withFilterContext filterContext: AnyObject?)
    {
        let ipAddress = GCDAsyncUdpSocket.hostFromAddress(address);
        let port = GCDAsyncUdpSocket.portFromAddress(address);
        
        guard let response = NSString(data: data, encoding: NSUTF8StringEncoding) else {
            DDLogDebug("UDP socket received unexpected data from \(ipAddress):\(port)", level: logLevel);
            return;
        }
        
        let components = response.componentsSeparatedByString(",");
        
        guard components.count >= 2 else {
            DDLogDebug("UDP socket failed to parse device details from response \(response) sent by \(ipAddress):\(port)", level: logLevel);
            return;
        }
        
        let deviceIP = components[0];
        let mac = components[1];
        
        let device = MDOnlineDevice(ip: deviceIP, mac: mac);
        
        delegate?.udpSocket(self, discoveredOnlineDevice: device);
    }
}
