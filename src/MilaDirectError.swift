//
//  MilaDirectError.swift
//  mila-direct
//
//  Created by Nikita Ivanchikov on 11/24/15.
//  Copyright © 2015 Nikita Ivanchikov. All rights reserved.
//


public enum MilaDirectError: ErrorType
{
	case InvalidUDPPassword
	
	case DeviceNotOnline(deviceUID: String);
}
